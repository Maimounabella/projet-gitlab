# Étape de clonage du référentiel
FROM alpine/git AS clone
WORKDIR /app

RUN rm -Rf /usr/share/nginx/html/*  

RUN git clone https://github.com/diranetafen/static-website-example.git

# Étape de production
FROM nginx:1.21.1
COPY --from=clone /app/static-website-example /usr/share/nginx/html

# Configuration Nginx
# COPY nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
